##Realizing the TCP capture##
**Cleaning everything before starting**  
rm(list=ls())

**Set the desired number of packets and start the capture**  
packetCount = 500  
TCPDumpCommand = paste("tcpdump -i any -n -c ",packetCount)  
TCPDump = system(TCPDumpCommand,intern = TRUE)  
 

**Separate each long strings into multiple elements**  
TCPDump=strsplit(TCPDump," ")  
  
  
##Computing total time of capturing  
**Converting the time value from string to a type that can be computed**  
options("digits.secs"=6)  
starttime=as.POSIXct(TCPDump[[50]][1],format = "%H:%M:%OS")  
stoptime=as.POSIXct(TCPDump[[length(TCPDump)]][1],format = "%H:%M:%OS")  
print(paste("It took",stoptime-starttime ,"seconds to collect", packetCount,"packets"))  
  
  
##Counting the destination IP addresses (different from own IP)  
ownIP="192.168.1.100"  
IPAddresses = vector()  
for (i in 1:length(TCPDump)){  
currentDestIP=TCPDump[[i]][5]  
if (!(grepl(ownIP,currentDestIP))){  
IPAddresses<-c(IPAddresses,currentDestIP)  
}  
}  
IPAddresses<-table(IPAddresses)  

**During this example I loaded Facebook because the main page is rich of content**  
pie(IPAddresses,main="IP connection when loading Facebook")