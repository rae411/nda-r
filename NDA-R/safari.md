---
title: "safarionlinebook"
author: "Havva Dagdelen"
date: "December 26, 2017"
output: html_document
---


#Havva Dagdelen
#setting the working directory
setwd("C:/Users/Havva/Documents/TT")

#load required library
library(RCurl)
library(XML)

#Create a list with all the files on the FTP site
list <- getURL("ftp://ftp.ncdc.noaa.gov/pub/data/gsod/2017",
               dirlistonly = TRUE)
#clean the list
Filelist <- strsplit(list, split = "\r\n")

#create a new directory where to dowload this files
DIR <- paste(getwd(),"NOAAFiles",sep="")
dir.create(DIR)


#loop to dowload the files
for (FileName in unlist(FileList)[1:10]) {
URL <- paste0("ftp://ftp.ncdc.noaa.gov/pub/data/gsod/2017", FileName)
download.file(URL, destfile = paste0(DIR,"/",FileName), method="auto",
              mode = "wb")
}
#dowload a compressed file 
URL <-"ftp://ftp.ncdc.noaa.gov/pub/data/gsod/2016/gsod_16.tar"
download.file(URL, destfile = paste0(DIR, "/gsod_2016.tar"),
              method="auto",mode="wb")

untar(paste0(getwd(),"/NOAAFiles/","gsod_2016.tar"),
       exdir = paste0(getwd(),"NOAAFiles"))
help(unzip)