---
title: "safari online book 2"
author: "Havva Dagdelen"
date: "December 27, 2017"
output: html_document
---


#set the URL with the CSV files

URL <-"https://earthquake.usgs.gov/earthquakes/feed/v1.0/csv.php"

#Load the CSV file
Data <- read.table(file=URL,
                   sep=",",
                   header=TRUE,
                   na.string="")
#help function
help(read.table)
#examining data
str(Data)

install.packages("RCurl")
install.packages("XML")
                
