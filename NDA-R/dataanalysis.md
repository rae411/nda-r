---
title: "hw"
author: "Havva Dagdelen"
date: "December 26, 2017"
output: html_document
---
 
 **Set the desired number of packets and start the capture**  
 packetCount = 500
 TCPDumpCommand = paste("tcpdump -i any -n -c ",packetCount)  
 TCPDump = system(TCPDumpCommand,intern = TRUE)  
  
 
 **Separate each long strings into multiple elements**  
 TCPDump=strsplit(TCPDump," ")  
   
   
-##Computing total time of capturing##  
+##Computing total time of capturing  
 **Converting the time value from string to a type that can be computed**  
 options("digits.secs"=6)  
Add a comment to this line
 starttime=as.POSIXct(TCPDump[[50]][1],format = "%H:%M:%OS")  
stoptime=as.POSIXct(TCPDump[[length(TCPDump)]][1],format = "%H:%M:%OS")  
 print(paste("It took",stoptime-starttime ,"seconds to collect", packetCount,"packets"))  
   
   
-##Counting the destination IP addresses (different from own IP)##  
+##Counting the destination IP addresses (different from own IP)  
 ownIP="78.28.208.75"  
 IPAddresses = vector()  
Add a comment to this line
 for (i in 1:length(TCPDump)){ 
 currentDestIP=TCPDump[[i]][5]  
 if (!(grepl(ownIP,currentDestIP))){  
 IPAddresses<-c(IPAddresses,currentDestIP)  
 }  
 }  
 IPAddresses<-table(IPAddresses)  
 
Add a comment to this line
 pie(IPAddresses,main="IP connection when loading Facebook")