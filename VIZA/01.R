x <- 1:10
y <- x^2
plot(x,y,type="l",col="pink",xlab="independent variable",ylab="dependent variable",main="Function graph")